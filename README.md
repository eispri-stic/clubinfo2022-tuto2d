# TUTO 2D 2022 - Doodle Jump

## 1.1 Introduction

Ce tutoriel a pour but de vous faire découvrir la programmation de jeux 2D en Godot. Nous allons donc créer un jeu de plateforme, inspiré de Doodle Jump, en utilisant les outils de Godot.

## 1.2 Prérequis

Vous devez avoir installé Godot sur votre ordinateur. Vous pouvez télécharger Godot sur le site officiel : https://godotengine.org/download

Vous devez aussi récuperer les assets du jeu. Vous pouvez les télécharger ici : https://drive.google.com/drive/folders/1J8-EgzyJSgRR4gYrOFm25zLwY2_8QaHA?usp=share_link

## 1.3 Création du projet

🎉 **C'est parti!** 🎉

Lancez Godot et cliquez sur "New Project". Choisissez un nom pour votre projet et un dossier où le projet sera créé. Vous pouvez laisser les autres paramètres par défaut.

## 1.4 Explication rapide de l'interface

![Interface](./images/godot_empty.png)

L'interface de Godot est composée de plusieurs parties :

- Un visuel de la scène au centre
- Une arborescence des noeuds de la scène à gauche
- Un panneau d'informations sur le noeud sélectionné à droite
- Un panneau de débogage en bas
- L'arborescence de votre projet en bas à gauche

## 1.5 Quelques configurations

### 1.5.1 Configuration du projet

Pour commencer, nous allons configurer l'éditeur pour avoir un format du jeu en vertical.

Pour cela, vous allez dans le menu "Projet" puis "Projet Settings". Dans la fenêtre qui s'ouvre, vous allez dans l'onglet "Displa/Window". Vous changez le width en `320` et le height en `640`. Pour ce premier jeu, on ne s'interessera pas à faire un jeu responsive. Mais vous pouvez le faire si vous le souhaitez.


### 1.5.2 Lancez votre jeu

Pour lancer votre jeu, vous clicker simplement sur le bouton "Play" en haut à droite de l'éditeur. Vous pouvez aussi appuyer sur la touche `F5` de votre clavier.

#  Création de la scène Game

Votre jeu contiendra plusieurs scènes. Une scène est un regroupement de noeuds. Un noeud est un objet dans la scène. Par exemple, un joueur, un ennemi, un décor, etc. Dans notre jeu, nous allons avoir une scène pour le menu, une scène pour le jeu, etc.

## 2.1 Création du noeud racine

La scène est composée de noeuds. Un noeud est un objet qui peut contenir d'autres noeuds. Il peut aussi contenir des scripts, des sprites, des sons, etc. Pour créer un noeud, cliquez sur le bouton "2D Scene" en haut à gauche de l'interface. Vous pouvez aussi cliquer sur le bouton "+" et choisir "Node2D" dans le menu déroulant.

Node2D signifie que nous allons créer un noeud 2D. Il existe aussi des noeuds 3D, qui permettent de créer des jeux en 3D et des noeuds Interface, qui permettent de créer des interfaces. (facile à deviner... 🤔)

![Node2D](./images/Node2D.png)

## 2.2 Sauvegarde de la scène

Pour sauvegarder la scène, faites un "ctrl+S".  
Puis creez un dossier `"scenes"` dans le dossier res://. 
Renommez votre scene en `"Game.tscn"`.  
Enfin, sauvegardez la scène dans le dossier scenes en cliquant sur "Save" dans le menu.

## 2.3 Création d'un joueur

Nous allons créer un joueur. Pour le moment il sera dans la scène Game, mais plus tard nous créerons une scène dédié à lui, histoire de faire plus propre 😉

Pour ajouter notre joueur, nous allons utiliser un noeud spécial, un `Kinematic2D`. C'est un Node2D qui va avoir plusieurs héritages d'autres noeuds integrés dans Godot. Il vous suffit de clicker sur "+" et de choisir KinematicBOdy2D dans la liste.

Vérifiez bien que le nouveau Node est bien enfant de Game ( comme dans l'image ) :

![firstChild](./images/firstChild.png)

```
"MaIs PoUrQuOi Il Y a Un WaRnInG ?" ⚠️  
```
Alors de 1 calmes toi c'est normal 👺, de 2 je t'invites à lire le message en mettant ta souris dessus.

Pour expliquer : notre objet à besoin d'une zone de collision pour fonctionner correctement. Ca tombe bien nous allons en créer un!

En suivant ce que vous avez vu au dessus, je vous invite à ajouter un noeud `CollisionShape2D` à votre `KinematicBody2D`.

> Vous pouvez aussi renommer vos noeuds, je vous recommande de le faire pour ne pas être perdu 👍

Votre arborescence devrait ressembler à ceci :

![collisionShape](./images/collisionShape.png)

```diff
"Et c'est quoi un CollisionShape2D ?" 🤔
```
C'est un objet qui va définir la zone de collision de notre objet. Il existe plusieurs types de `CollisionShape2D`, mais pour l'instant nous allons utiliser un Rectangle.

```diff
"Encore un warning ?" ⚠️
```

Oui, et c'est toujours normal, nous n'avons pas encore défini notre rectangle de collision. Pour cela, il faut clicker sur votre `CollisionShape2D`. Vous verrez ensuite sur la droit de votre écran un panneau d'informations sur votre objet. Dans ce panneau, vous pouvez changer les propriétés de votre objet. Dans notre cas, nous allons changer la propriété `Shape` en `new RectangleShape2D`.

![rectangleShape](./images/rectangleShape.png)

Pour le moment notre rectangle est de taille 20px par 20px. Avant de modifier sa taille, nous allons ajouter un sprite à notre joueur. Pour cela, nous allons ajouter un noeud `Sprite` à notre `KinematicBody2D`.

> Un sprite est une image qui va être affichée à l'écran. Il peut être animé, etc.
Ce sera notre joueur.

![sprite](./images/sprite.png)

> N'oubliez pas de mettre le dossier assets dans le dossier res:// pour récuperer l'image du joueur. 👍


Pour le moment, notre sprite est vide. Nous allons donc ajouter une image à notre sprite. Pour cela, nous allons mettre notre image dans le dossier `res://assets/` et nous allons ajouter un `Texture` à notre `Sprite`.

Pour vous faciliter la vie vous pouvez directement glisser l'image depuis la fenetre `FileSystem` vers la fenetre `Inspector` :

![texture](./images/texture.png)

Vous pouvez maintenant voir votre joueur dans la scène. Il est tout petit, mais c'est normal. Nous allons maintenant modifier la taille de notre rectangle de collision et de notre sprite.

> Conseil pratique :  
Avoir une cohérence sur les tailles est assez compliqué, je vous recommande d'activer le `Smart Snap` et le `Grid Snap` dans le menu du dessus au milieu (voir image ci-dessous).

> Surtout modifier bien la taille de votre sprite et de votre rectangle de collision pour qu'ils soient de la même taille. Et via l'interface visuelle, et non avec l'onglet `Transform` de l'inspecteur. Qui servira à autre chose.

![snap](./images/gridSnap.png)

Autre modification : 

Etant donnée que notre joueur va detecter les collisions au niveau des pieds, nous n'avons pas besoin d'avoir un collisionShape en rectangle pour tout notre joueur,

Je vous invites donc à modifier le shape de collision par un segment.

![segment](./images/segment.png)

Deplacez ensuite votre `Player` vers le centre de la scene. Puis lancer le jeu avec la touche F5.

```
"Rien ne bouge" 🤔
```

C'est normal, nous n'avons pas encore défini les mouvements de notre joueur. Nous allons donc le faire maintenant.

## 2.4 Déplacement du joueur

Pour déplacer notre joueur, nous allons utiliser un script. Pour cela, nous allons ajouter un noeud `Script` à notre `KinematicBody2D`.

![script](./images/script.png)

> Un script est un fichier qui va contenir du code. Il va nous permettre de faire des actions sur notre objet.

> Le langage utilisé est le GDScript, un langage de script basé sur Python.

En reliant notre script à notre `KinematicBody2D`, nous allons pouvoir modifier notre noeud plus facilement.

Je vous invites à renommer votre script en `Player.gd` pour plus de clarté. Et de le mettre dans le dossier `res://scripts/`.

> Pour plus de clareté, vous allez creer votre scene Player.tscn et deplacer votre Player dedans.
Pensez ensuite à glisser votre nouvelle scene dans la scène principale.

Une fonction dans Godot s'ecrit de la maniere suivante :

```gdscript
func nomDeLaFonction():
    # Code de la fonction
```

Certaines fonctions sont déjà définies dans Godot, comme `func _ready():` qui est appelée lorsque l'objet est prêt à être utilisé.gridSnap.png  
On utilisera aussi `func _physics_process(delta):` qui est appelée à chaque frame.

Pour commencer nous allons définir les variables de notre joueur. Pour cela, nous allons ajouter une suite de var dans notre script.

```gdscript
var masse = 1 #kg
var vitesse = 300 #px/s
var velocity = Vector2(0, 0)
```

> Une variable est une valeur qui peut changer au cours du temps.

> La masse est en `kg` (kilogramme) et la vitesse en `px/s` (pixel par seconde).

> Une `Vector2` est un vecteur à deux dimensions. Il est composé de deux valeurs : `x` et `y`. très utile pour les mouvements.

> Pour plus d'informations sur les variables, vous pouvez consulter la documentation de Godot.

Nous allons maintenant définir les mouvements de notre joueur. Pour cela, nous allons utiliser la fonction `_physics_process(delta)`.

```gdscript
func _physics_process(delta):
    self.position.x += 10 * delta
```

> La fonction `_physics_process(delta)` est appelée à chaque frame. Elle prend en paramètre `delta` qui est le temps écoulé depuis la dernière frame.

> `self` est une variable qui représente l'objet sur lequel on travaille.

> `self.position` est la position de l'objet sur lequel on travaille.

> à chaque frame, on ajoute 10 pixels à la position sur l'axe `x` de l'objet.

Lancez votre jeu et regardez ce qu'il se passe.

```
"Le joueur se déplace à droite" 🤔
```

Oui! il faut bien comprendre que l'axe `x` est l'axe horizontal et que l'axe `y` est l'axe vertical.
la base des deux axes se situe en "haut" à "gauche" de l'ecran. Plus on va vers la droite, plus la valeur de `x` augmente. **Plus on va vers le bas, plus la valeur de `y` augmente.**

> Pour l'axe y, il faut bien retenir ça pour notre jeu.

C'est bien sympa de se deplacer à droite, mais on aimerais avoir le contrôle sur notre joueur.

Allez dans le menu `Project` et cliquez sur `Project Settings`.

Allez ensuite dans l'onglet `Input Map`.

> `Input Map` regroupe l'ensemble des touches du clavier, de la souris et des manettes. Vous pouvez creer des groupes de touches pour les utiliser dans votre jeu. 

Vous allez créer deux nouvelles Actions, `move_right` et `move_left`.

![inputMap](./images/inputmap.png)

> Lorsque nous ferons référence à ces actions, nous utiliserons le nom que vous avez donné à l'Action.

Retournons sur notre script `Player.gd`.

Nous allons maintenant définir les mouvements de notre joueur. Pour cela, nous allons utiliser une  fonction `_unhandled_input(event)`.

```gdscript
func _unhandled_input(event):
	if(Input.is_action_pressed("move_right")):
		self.action_pressed = true
		self.velocity.x = speed
	elif(Input.is_action_pressed("move_left")):
		self.action_pressed = true
		self.velocity.x = -speed
	else:
        self.velocity.x = 0
		self.action_pressed = false
```

> La fonction `_unhandled_input(event)` est appelée à chaque fois qu'une touche est appuyée ou relachée. Elle prend en paramètre `event` qui est l'événement qui vient d'être déclenché.

> `Input` est une classe qui permet de récupérer les actions définies dans `Input Map`.

> `Input.is_action_pressed("move_right")` renvoie `true` si l'action `move_right` est enfoncée. Même chose pour `move_left`.

> pour chaque action, on modifie la valeur de `velocity.x` en fonction de la touche appuyée.

Il ne vous reste plus qu'a modifier la fonction `_physics_process(delta)` pour que le joueur se déplace en fonction de la valeur de `velocity`.

Je vous laisser essayer par vous même. Rien de bien compliqué.

Pour vous amusez, vous pouvez modifier la vitesse de déplacement de votre joueur. Vous aurez besoin de modifier la valeur de `speed` dans votre script.
Ajustez comme vous le souhaitez.

Plus qu'a ajouter un peu de gravité à notre joueur.

D'abord un petit rappel sur la formule de la gravité.

```
G = masse * gravité

masse : masse du joueur en kg
gravité : constante gravitationnelle en m/s² soit 9.81 m/s²
```

Nous allons donc ajouter une nouvelle variable `gravite` à notre script.

```gdscript
var gravite = 9.81 #m/s²
```

Nous allons maintenant modifier la fonction `_physics_process(delta)` pour que le joueur tombe.

```gdscript
func _physics_process(delta):
    self.velocity.y += gravite * delta
    self.position += self.velocity * delta
```

> `self.velocity.y` est la vitesse de déplacement sur l'axe `y` de l'objet.

> on modifie la valeur de `self.velocity.y` en fonction de la gravité. Puis on applique cette valeur à la position de l'objet.

Ok tout va bien jusque là ? Si vous avez bien suivi, vous devriez avoir un joueur qui tombe.

Pour le moment, notre joueur peut sortir de l'ecran sur l'axe horizontal.
Mais pour ce tutoriel, nous allons nous contenter de ça. Néenmoins, vous pouvez essayer de le faire vous même. Soit par une teleportation, soit par une collision.

# Création d'une tuile

Nous allons maintenant créer la tuile de notre jeu.

Ces tuiles seront des `Sprite` avec un `CollisionShape2D` en enfant.

Ils serviront à la collision de notre joueur.

Créez votre scène `Tuile.tscn` et ajoutez un `Sprite` et un `CollisionShape2D` en enfant.

![tuile](./images/tuile.png)

> Comme pour le joueur, vous utiliserez un segment pour la collision. Pensez bien à centrer votre objet sur l'axe x

Importez une tuile dans votre scene principal, ce sera temporaire le temps d'implementer les collisions.

![temp](./images/temp.png)

Si vous lancez votre jeu, vous devriez voir votre joueur traverser la plateforme. C'est normal, nous n'avons pas encore implementé les collisions.

Pour cela, retournez sur votre scène `Player.tscn` et modifiez le script `Player.gd` pour qu'il gère les collisions.

```gdscript
func _physics_process(delta):
	#applications
	self.velocity.y += (self.masse * Globals.gravity)
	if velocity.y < 0: #si le joueur est en train de monter, il est en libre mouvement
		self.position += velocity * delta
	else: #si le joueur descend, il peut entrer en collision avec les objets
		var collision = move_and_collide(velocity * delta)
		if collision:
			self.velocity.y = -400
```

> `move_and_collide(velocity * delta)` est une fonction qui permet de déplacer l'objet et de détecter les collisions. Elle renvoie `null` si il n'y a pas de collision. Sinon, elle renvoie un objet `CollisionObject2D` qui contient les informations sur la collision.

> Le principe est simple, si le joueur monte, on se deplace sans prendre en compte les collisions, sinon on déplace le joueur et on regarde si il y a une collision. Si oui, on inverse la vitesse de déplacement sur l'axe `y`. Ce qui a pour effet de faire rebondir le joueur.

> Pour commencer c'est bien mais il existe des méthodes plus optimisées pour gérer les collisions. Vous pouvez essayer de les implémenter vous même.

Pour plus d'optimisation, je vous invite à créer une fonction `bounce()` qui sera appelée à chaque fois qu'il y a une collision.

cette fonction sera donc appelée dans la fonction `_physics_process(delta)` et dans la fonction `_ready()`.


# Création "des" tuiles

Pouvoir ajouter une tuile c'est bien, mais pouvoir en ajouter plusieurs c'est mieux. Et ce n'est pas avec nos petites mains qu'on va pouvoir en ajouter 1000 de manière aléatoire.

Nous allons donc créer un script qui va nous permettre de générer des tuiles de manière aléatoire.

## Création du script

Dans un monde idéal, Tuiles serait simplement une classe gd qui contiendrait les fonctions de génération de tuiles. Mais pour ce tutoriel, nous allons créer une scène qui contiendra notre script.

Créez une scène `Tuiles.tscn` et ajoutez un `Node2D` en enfant.

Vous y ajoutez un script `Tuiles.gd` et vous ajoutez un `Node2D` en enfant.

dans le script vous aurez 2 fonctions :

`createTyuile(y, color)` qui va créer une tuile à la position y avec la couleur color.

`createTuiles(rand)` qui va créer des tuiles aléatoirement.
`rand` et un number qui va définir la difficulté du jeu. Plus il est grand, moins il y aura de chance d'avoir une tuile sur un pas de hauteur (soit 1/rand chance).

Je vous laisses le soin de créer ces fonctions. D'abord CreateTuile, puis CreateTuiles, qui appelera CreateTuile.

Je tiens à ce que vous cherchez aussi par vous même, mais si vous bloquez, n'hesitez pas à venir me voir. Je vous aiderai avec plaisir 🙋.
Et si vous avancez ce tutoriel depuis chez vous. Vous pouvez me joindre via discord : `Durbo#3108`

Pour finir cette première partie du tutoriel. Vous aurez juste à glisser votre scène Tuiles.tscn dans votre scène principale et à appeler la fonction `createTuiles()` dans la fonction `_ready()` de votre scène principale.

# Conclusion

Nous avons donc créer un joueur qui peut sauter et des tuiles qui peuvent être générées aléatoirement.

Nous avons aussi vu comment gérer les collisions.

Dans la prochaine partie, nous allons implementer :
- un système de score et de mort.
- une caméra qui suit le joueur.
- une génération de tuiles au fûr et à mesure que le joueur avance.

Vous pouvez avancer si vous le souhaitez, voici une liste de choses à faire qui ne seront pas dans le tutoriel :
- ajouter un menu de démarrage
- ajouter un menu de pause
- téléporter le joueur lorsqu'il sort de l'écran
- ajouter un son lorsqu'il y a une collision
- ajouter une animation sur le personnage
- créer des tuiles spéciales qui font des actions particulières (ex : tuile qui fait sauter plus haut)
- ajouter des ennemis
- ajouter des bonus

Vous pouvez aussi faire le tutoriel Flappy Bird, créé avec soins par Arthur Bajt, qui est très bien fait et qui vous permettra de bien réviser ce que nous avons vu dans ce tutoriel.